Feature: Check website header

Scenario: Website has Title
  Given I visit the website "https://atermonas.com" as a crawler
  Then it should have a title tag "Atermonas"
