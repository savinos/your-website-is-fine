Given("I visit the website {string} as a crawler") do |string|
  @page = MetaInspector.new(string)
end

Then("it should has title tag") do
  expect(@page.title).not_to eql("")
  puts @page.title
end

Then("it should have a title tag {string}") do |string|
  expect(@page.title).to eql(string)
end


Then("it should not be empty") do
  pending # Write code here that turns the phrase above into concrete actions
end
